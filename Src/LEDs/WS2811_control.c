#include "stm32f1xx_hal.h"

#include "WS2811_control.h"

uint8_t data[144] = {0, };
uint8_t zeros[6] = {0, };

void WS2811_push(uint8_t red, uint8_t green, uint8_t blue)
{
  HAL_SPI_Transmit_IT(&hspi2, zeros, 6);
  
  // 8 SPI bytes to emulate 1 WS2811 byte. Each WS2811 bit coded using 8 SPI bits.
  // WS: 0 => SPI: 1100 0000
  // WS: 1 => SPI: 1111 0000
  // 24 bytes -> 1 LED RGB
  // 6 LEDs -> 144 bytes
  WS_Put_NonBlocking(red, green, blue);
}

static void WS_Put_NonBlocking(uint8_t ws_red, uint8_t ws_green, uint8_t ws_blue)
{
  uint8_t i, shifted;
  int8_t j;
  
  for (j = 7; j > -1; --j) {
    shifted = ws_red >> j;
    data[7-j] = ((shifted & 0x1) ? 0xF0 : 0xC0);
  }
  
  for (j = 7; j > -1; --j) {
    shifted = ws_green >> j;
    data[15-j] = ((shifted & 0x1) ? 0xF0 : 0xC0);
  }
  
  for (j = 7; j > -1; --j) {
    shifted = ws_blue >> j;
    data[23-j] = ((shifted & 0x1) ? 0xF0 : 0xC0);
  }
  
  
  for (i = 24; i <= 120; i = i + 24) {
    for (j = 0; j < 24; ++j) {
      // Copying first 24 bytes (24 bytes for 1 LED).
      data[i + j] = data[j];
    }
  }
  
  
  HAL_StatusTypeDef result = HAL_SPI_Transmit_IT(&hspi2, data, 144);
}
