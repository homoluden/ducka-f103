#ifndef __WS2811_control_H__
#define __WS2811_control_H__

//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

#include "spi.h"


// *** DEFINES

/// defines

void WS2811_push(uint8_t red, uint8_t green, uint8_t blue);
static void WS_Put_NonBlocking(uint8_t ws_red, uint8_t ws_green, uint8_t ws_blue);


#ifdef __cplusplus
}
#endif

#endif // __WS2811_control_H__
