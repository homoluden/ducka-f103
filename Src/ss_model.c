#include "ss_model.h"

float SS_step(SS_Model *ss, float *state, float tgt){
    double xn1 = ss->A[0] * state[0] + ss->A[1] * state[1] + ss->B[0] * tgt;
    double xn2 = ss->A[2] * state[0] + ss->A[3] * state[1] + ss->B[1] * tgt;

    state[0] = xn1;
    state[1] = xn2;

    return (float)(ss->C[0] * state[0] + ss->C[1] * state[1] + ss->D * tgt);
}
