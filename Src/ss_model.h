#ifndef __SS_MODEL_H__
#define __SS_MODEL_H__

//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

// *** DEFINES ***

typedef struct {
  double A[4];
  double B[2];
  double C[2];
  double D;
}SS_Model;

/// defines

// *** PROTOTYPES ***

float SS_step(SS_Model *ss, float *state, float tgt);

/// prototypes

#ifdef __cplusplus
}
#endif

#endif // __SS_MODEL_H__
