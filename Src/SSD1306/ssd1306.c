/*
@file     ssd1306.c
@author   K. Townsend (microBuilder.eu)

@section DESCRIPTION

Driver for 128x64 OLED display based on the SSD1306 controller.

This driver is based on the SSD1306 Library from Limor Fried
(Adafruit Industries) at: https://github.com/adafruit/SSD1306  

@section LICENSE

Software License Agreement (BSD License)

Copyright (c) 2010, microBuilder SARL
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holders nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_gpio.h"
#include "spi.h"

#include "ssd1306.h"

void ssd1306SendByte(uint8_t byte);
void OLED_SetPos(uint8_t x, uint8_t y);

#define CMD(c)        do { HAL_GPIO_WritePin(SSD1306_DC_PORT, SSD1306_DC_PIN, GPIO_PIN_RESET); \
ssd1306SendByte( c ); \
                         } while (0);
#define DATA(c)       do { HAL_GPIO_WritePin(SSD1306_DC_PORT, SSD1306_DC_PIN, GPIO_PIN_SET); \
ssd1306SendByte( c ); \
                         } while (0);

//static uint8_t buffer[SSD1306_LCDWIDTH * SSD1306_LCDHEIGHT / 8];

/**************************************************************************/
/* Private Methods                                                        */
/**************************************************************************/

void delay(uint32_t count)
{
  while(--count != 0){}
}

/**************************************************************************/
/*! 
@brief SPI write

@param[in]  byte
The byte to send
*/
/**************************************************************************/
void ssd1306SendByte(uint8_t byte)
{
  unsigned char i;
  for(i=0;i<8;i++)
  {
    HAL_GPIO_WritePin(SSD1306_SCL_PORT, SSD1306_SCL_PIN, GPIO_PIN_RESET);
    
    delay(2);
    
    if((byte << i) & 0x80)
      HAL_GPIO_WritePin(SSD1306_SDA_PORT, SSD1306_SDA_PIN, GPIO_PIN_SET);
    else
      HAL_GPIO_WritePin(SSD1306_SDA_PORT, SSD1306_SDA_PIN, GPIO_PIN_RESET);
    
    HAL_GPIO_WritePin(SSD1306_SCL_PORT, SSD1306_SCL_PIN, GPIO_PIN_SET);
    
    delay(4);
  }
}

/**************************************************************************/
/* Public Methods                                                         */
/**************************************************************************/

/**************************************************************************/
/*! 
@brief Initialises the SSD1306 LCD display
*/
/**************************************************************************/
void ssd1306Init(uint8_t vccstate)
{
  // Reset the LCD
  HAL_GPIO_WritePin(SSD1306_RST_PORT, SSD1306_RST_PIN, GPIO_PIN_RESET);
  HAL_Delay(10);
  HAL_GPIO_WritePin(SSD1306_RST_PORT, SSD1306_RST_PIN, GPIO_PIN_SET);
  HAL_Delay(10);
  
  // Initialisation sequence
  CMD(0xae);
  CMD(0xae);//--turn off oled panel
  CMD(0x00);//---set low column address
  CMD(0x10);//---set high column address
  CMD(0x40);//--set start line address  Set Mapping RAM Display Start Line (0x00~0x3F)
  CMD(0x81);//--set contrast control register
  CMD(0xcf); // Set SEG Output Current Brightness
  CMD(0xa1);//--Set SEG/Column Mapping     0xa0,0xa1
  CMD(0xc8);//Set COM/Row Scan Direction   0xc0,0xc8
  CMD(0xa6);//--set normal display
  CMD(0xa8);//--set multiplex ratio(1 to 64)
  CMD(0x3f);//--1/64 duty
  CMD(0xd3);//-set display offset	Shift Mapping RAM Counter (0x00~0x3F)
  CMD(0x00);//-not offset
  CMD(0xd5);//--set display clock divide ratio/oscillator frequency
  CMD(0x80);//--set divide ratio, Set Clock as 100 Frames/Sec
  CMD(0xd9);//--set pre-charge period
  CMD(0xf1);//Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
  CMD(0xda);//--set com pins hardware configuration
  CMD(0x12);
  CMD(0xdb);//--set vcomh
  CMD(0x40);//Set VCOM Deselect Level
  CMD(0x20);//-Set Page Addressing Mode (0x00/0x01/0x02)
  CMD(0x02);//
  CMD(0x8d);//--set Charge Pump enable/disable
  CMD(0x14);//--set(0x10) disable
  CMD(0xa4);// Disable Entire Display On (0xa4/0xa5)
  CMD(0xa6);// Disable Inverse Display On (0xa6/a7) 
  CMD(0xaf);//--turn on oled panel
  
  OLED_Fill(0x00);
  OLED_SetPos(0,0);
}

void ssd1306TurnOn(void)
{
  // Enable the OLED panel
  CMD(SSD1306_DISPLAYON);
  // FIXME: Set the flag enabling Frame Buffer refresh
}

void ssd1306TurnOff(void)
{
  // FIXME: Clear the flag enabling Frame Buffer refresh
  // Disable the OLED panel
  CMD(SSD1306_DISPLAYOFF);
}

/**************************************************************************/
/*! 
@brief Draws a single pixel in image buffer

@param[in]  x
The x position (0..127)
@param[in]  y
The y position (0..63)
*/
/**************************************************************************/
void ssd1306DrawPixel(uint8_t x, uint8_t y) 
{
  if ((x >= SSD1306_LCDWIDTH) || (y >= SSD1306_LCDHEIGHT))
    return;
  
  BMP1[x+ ((uint8_t)(y/8))*SSD1306_LCDWIDTH] |= (1 << y%8);
}

/**************************************************************************/
/*! 
@brief Clears a single pixel in image buffer

@param[in]  x
The x position (0..127)
@param[in]  y
The y position (0..63)
*/
/**************************************************************************/
void ssd1306ClearPixel(uint8_t x, uint8_t y) 
{
  if ((x >= SSD1306_LCDWIDTH) || (y >= SSD1306_LCDHEIGHT))
    return;
  
  BMP1[x+ (y/8)*SSD1306_LCDWIDTH] &= ~(1 << y%8); 
}

/**************************************************************************/
/*! 
@brief Gets the value (1 or 0) of the specified pixel from the buffer

@param[in]  x
The x position (0..127)
@param[in]  y
The y position (0..63)

@return     1 if the pixel is enabled, 0 if disabled
*/
/**************************************************************************/
uint8_t ssd1306GetPixel(uint8_t x, uint8_t y)
{
  if ((x >= SSD1306_LCDWIDTH) || (y >=SSD1306_LCDHEIGHT)) return 0;
  return BMP1[x+ (y/8)*SSD1306_LCDWIDTH] & (1 << y%8) ? 1 : 0;
}

/**************************************************************************/
/*! 
@brief Clears the screen
*/
/**************************************************************************/
void ssd1306ClearScreen() 
{
  memset(BMP1, 0, 1024);
}

/**************************************************************************/
/*!
@brief Draws a line
*/
/**************************************************************************/
void ssd1306DrawLine(uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1) {
  uint8_t dy, dx;
  uint8_t addx, addy;
  int16_t P, diff, i;
  
  if (x1 >= x0) {
    dx = x1 - x0;
    addx = 1;
  } else {
    dx = x0 - x1;
    addx = -1;
  }
  if (y1 >= y0) {
    dy = y1 - y0;
    addy = 1;
  } else {
    dy = y0 - y1;
    addy = -1;
  }
  
  if (dx >= dy) {
    dy *= 2;
    P = dy - dx;
    diff = P - dx;
    
    for(i=0; i<=dx; ++i) {
      BMP1[x0+ (y0/8)*SSD1306_LCDWIDTH] |= ~(1 << y0%8);
      if (P < 0) {
        P  += dy;
        x0 += addx;
      } else {
        P  += diff;
        x0 += addx;
        y0 += addy;
      }
    }
  } else {
    dx *= 2;
    P = dx - dy;
    diff = P - dy;
    
    for(i=0; i<=dy; ++i) {
      BMP1[x0+ (y0/8)*SSD1306_LCDWIDTH] |= ~(1 << y0%8);
      if (P < 0) {
        P  += dx;
        y0 += addy;
      } else {
        P  += diff;
        x0 += addx;
        y0 += addy;
      }
    }
  }
}

/**************************************************************************/
/*!
@brief Draws a circle
*/
/**************************************************************************/
void ssd1306DrawCircle(uint8_t x, uint8_t y, uint8_t radius) {
  int16_t a, b, P;
  
  a = 0;
  b = radius;
  P = 1 - radius;
  
  do {
    BMP1[(x+a)+ ((y+b)/8)*SSD1306_LCDWIDTH] |= ~(1 << (y+b)%8);
    BMP1[(x+b)+ ((y+a)/8)*SSD1306_LCDWIDTH] |= ~(1 << (y+a)%8);
    BMP1[(x-a)+ ((y+b)/8)*SSD1306_LCDWIDTH] |= ~(1 << (y+b)%8);
    BMP1[(x-b)+ ((y+a)/8)*SSD1306_LCDWIDTH] |= ~(1 << (y+a)%8);
    BMP1[(x+b)+ ((y-a)/8)*SSD1306_LCDWIDTH] |= ~(1 << (y-a)%8);
    BMP1[(x+a)+ ((y+b)/8)*SSD1306_LCDWIDTH] |= ~(1 << (y+b)%8);
    BMP1[(x-a)+ ((y-b)/8)*SSD1306_LCDWIDTH] |= ~(1 << (y-b)%8);
    BMP1[(x-b)+ ((y-a)/8)*SSD1306_LCDWIDTH] |= ~(1 << (y-a)%8);
    if (P < 0)
      P += 3 + 2*a++;
    else
      P += 5 + 2*(a++ - b--);
  } while(a <= b);
}

/**************************************************************************/
/*!
@brief Draws a filled rectangle
*/
/**************************************************************************/
void ssd1306FillArea(uint8_t x, uint8_t y, uint8_t cx, uint8_t cy, uint8_t color) {
  uint8_t x0, x1, y1;
  
  x0 = x;
  x1 = x + cx;
  y1 = y + cy;
  for(; y < y1; y++)
    for(x = x0; x < x1; x++)
    {
      if(color)
      {
        ssd1306DrawPixel(x, y);
      }
      else
        ssd1306ClearPixel(x, y);
    }
}

/**************************************************************************/
/*! 
@brief Renders the contents of the pixel buffer on the LCD
*/
/**************************************************************************/
void ssd1306Refresh(void) 
{
  OLED_BMP(0,0,128,8,(unsigned char *)BMP1); // FIXME: Check if refresh actually needed
}

/**************************************************************************/
/*!
@brief  Draws a single graphic character using the supplied font
*/
/**************************************************************************/
void ssd1306DrawChar(uint8_t x, uint8_t y, uint8_t c, const struct FONT_DEF font)
{
  uint8_t col, column[8];

  // Check if the requested character is available
  if ((c >= font.u8FirstChar) && (c <= font.u8LastChar))
  {
    // Retrieve appropriate columns from font data
    for (col = 0; col < font.u8Width; col++)
    {
      column[col] = font.au8FontTable[((c - 32) * font.u8Width) + col];    // Get first column of appropriate character
    }
  }
  else
  {
    // Requested character is not available in this font ... send a space instead
    for (col = 0; col < font.u8Width; col++)
    {
      column[col] = 0xFF;    // Send solid space
    }
  }

  // Render each column
  uint16_t xoffset, yoffset;
  for (xoffset = 0; xoffset < font.u8Width; xoffset++)
  {
    for (yoffset = 0; yoffset < (font.u8Height + 1); yoffset++)
    {
      uint8_t bit = 0x00;
      bit = (column[xoffset] << (8 - (yoffset + 1)));     // Shift current row bit left
      bit = (bit >> 7);                     // Shift current row but right (results in 0x01 for black, and 0x00 for white)
      if (bit)
      {
        ssd1306DrawPixel(x + xoffset, y + yoffset);
      }
    }
  }
}

/**************************************************************************/
/*!
@brief  Draws a string using the supplied font data.

@param[in]  x
Starting x co-ordinate
@param[in]  y
Starting y co-ordinate
@param[in]  text
The string to render
@param[in]  font
Pointer to the FONT_DEF to use when drawing the string

@section Example

@code 

#include "drivers/lcd/bitmap/ssd1306/ssd1306.h"
#include "drivers/lcd/smallfonts.h"

// Configure the pins and initialise the LCD screen
ssd1306Init();

// Render some text on the screen
ssd1306DrawString(1, 10, "5x8 System", Font_System5x8);
ssd1306DrawString(1, 20, "7x8 System", Font_System7x8);

// Refresh the screen to see the results
ssd1306Refresh();

@endcode
*/
/**************************************************************************/
void ssd1306DrawString(uint8_t x, uint8_t y, const char *text, struct FONT_DEF font)
{
  // FIXME: Add lock here to protect Frame Buffer from Race Condition
  // OR make calling thread (Command Parse task) prioritized higher to avoid interruption 
  // by FrameBuffer refresh thread
  uint8_t l;
  for (l = 0; l < strlen(text); l++)
  {
    ssd1306DrawChar(x + (l * (font.u8Width + 1)), y, text[l], font);
  }
}

/**************************************************************************/
/*!
@brief  Shifts the contents of the frame buffer up the specified
number of pixels

@param[in]  height
The number of pixels to shift the frame buffer up, leaving
a blank space at the bottom of the frame buffer x pixels
high

@section Example

@code 

#include "drivers/lcd/bitmap/ssd1306/ssd1306.h"
#include "drivers/lcd/smallfonts.h"

// Configure the pins and initialise the LCD screen
ssd1306Init();

// Enable the backlight
ssd1306BLEnable();

// Continually write some text, scrolling upward one line each time
while (1)
{
// Shift the buffer up 8 pixels (adjust for font-height)
ssd1306ShiftFrameBuffer(8);
// Render some text on the screen with different fonts
ssd1306DrawString(1, 56, "INSERT TEXT HERE", Font_System5x8);
// Refresh the screen to see the results
ssd1306Refresh();    
// Wait a bit before writing the next line
systicknilThdSleepMilliseconds(1000);
    }

@endcode
*/
/**************************************************************************/
//void ssd1306ShiftFrameBuffer( uint8_t height )
//{
//  if (height == 0) return;
//  if (height >= SSD1306_LCDHEIGHT)
//  {
//    // Clear the entire frame buffer
//    ssd1306ClearScreen();
//    return;
//  }
//
//  // This is horribly inefficient, but at least easy to understand
//  // In a production environment, this should be significantly optimised
//
//  uint8_t y, x;
//  for (y = 0; y < SSD1306_LCDHEIGHT; y++)
//  {
//    for (x = 0; x < SSD1306_LCDWIDTH; x++)
//    {
//      if ((SSD1306_LCDHEIGHT - 1) - y > height)
//      {
//        // Shift height from further ahead in the buffer
//        ssd1306GetPixel(x, y + height) ? ssd1306DrawPixel(x, y) : ssd1306ClearPixel(x, y);
//      }
//      else
//      {
//        // Clear the entire line
//        ssd1306ClearPixel(x, y);
//      }
//    }
//  }
//}

/****************************************************************************/
/*             DRAW BMP                                                     */

void OLED_Fill(uint8_t bmp_dat)
{
  uint8_t y,x;
  for(y=0;y<8;y++)
  {
    CMD(0xb0+y);
    CMD(0x01);
    CMD(0x10);
    for(x=0;x<SSD1306_LCDWIDTH;x++)
    {
      DATA(bmp_dat);
    }
  }
}

void OLED_CLS(void)
{
  OLED_Fill(0x00);
}

void OLED_SetPos(uint8_t x, uint8_t y)
{
  CMD(0xb0 + y);
  CMD(((x&0xf0)>>4)|0x10);
  CMD((x&0x0f)|0x01);
}

//void OLED_6x8Str(unsigned char x, unsigned char y, unsigned char ch[])
//{
//  unsigned char c=0,i=0,j=0;
//  while (ch[j]!='\0')
//  {
//    c = ch[j]-32;
//    if(x>126)
//    {
//      x=0;y++;
//    }
//    OLED_SetPos(x,y);
//    for(i=0;i<6;i++)
//    {
//      DATA(F6x8[c][i]);
//    }
//    x+=6;
//    j++;
//  }
//}
//
//void OLED_8x16Str(unsigned char x, unsigned char y, unsigned char ch[])
//{
//  unsigned char c=0,i=0,j=0;
//  while (ch[j]!='\0')
//  {
//    c =ch[j]-32;
//    if(x>120)
//    {
//      x=0;y++;
//    }
//    OLED_SetPos(x,y);
//    for(i=0;i<8;i++)
//    {
//      DATA(F8X16[c*16+i]);
//    }
//    OLED_SetPos(x,y+1);
//    for(i=0;i<8;i++)
//    {
//      DATA(F8X16[c*16+i+8]);
//    }
//    x+=8;
//    j++;
//  }
//}

void OLED_BMP(unsigned char x0, unsigned char y0, unsigned char x1, unsigned char y1, unsigned char BMP[])
{
  unsigned int j=0;
  unsigned char x,y;
  
  if(y1%8==0)
  {
    y=y1/8;
  }
  else
    y=y1/8+1;
  for(y=y0;y<y1;y++)
  {
    OLED_SetPos(x0,y);
    for(x=x0;x<x1;x++)
    {
      DATA(BMP[j++]);
    }
  }
}
