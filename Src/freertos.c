/**
  ******************************************************************************
  * File Name          : freertos.c
  * Date               : 22/03/2015 22:43:11
  * Description        : Code for freertos applications
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     

#include <stdio.h>
#include <string.h>

#include "usart.h"
#include "ss_model.h"
#include "SSD1306/ssd1306.h"
#include "SSD1306/lcd_helper.h"

/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId defaultTaskHandle;

/* USER CODE BEGIN Variables */

extern 

#define COERCE_VALUE(lb, val, rb) ((lb) > (val)) ? (lb) : (((rb) > (val)) ? (val) : (rb))

/* COMMANDS BEGIN ------------------------------------------------------------*/
char *RGB_CMD = "rgb";
char *CLS_CMD = "cls";
char *TXT_CMD = "txt";
/* commands end ------------------------------------------------------------*/

uint8_t cmdReceiveStatus = 0; // 0 - NotStarted, 1 - InProgress, 2 - Completed

uint8_t cmdBuf[100] = {0, };

uint8_t tgtRed = 128, tgtGreen = 128, tgtBlue = 128;

static SS_Model animSS = {
  .A = {0.01549, -1.219, 0.002438,  0.9906},
  .B = {0.002438, 1.881e-05},
  .C = {5, 500},
  .D = 0
};

float redState[2] = {0, }, greenState[2] = {0, }, blueState[2] = {0, };

/* BEGIN Task Handles -------------------------------------------------------------*/
char* LEDsToggle_TaskName = "LEDsTgl";
osThreadId LEDsToggle_TaskHandle;

char* SSAnim_TaskName = "SSAnim";
osThreadId SSAnim_TaskHandle;

char* ReadCommand_TaskName = "RdCmd";
osThreadId ReadCommand_TaskHandle;

char* FrameBufferUpd_TaskName = "FBUpd";
osThreadId FrameBufferUpd_TaskHandle;

/* END Task Handles -------------------------------------------------------------*/

/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartDefaultTask(void const * argument);

/* USER CODE BEGIN FunctionPrototypes */
extern void WS2811_push(uint8_t red, uint8_t green, uint8_t blue);
void TaskLEDsToggle(const void *param);
void SSAnim(const void *param);
void ReadCommand(const void *param);
void FBUpdate(const void *param);
void ParseAndExecuteCommand(char *cmdString);
/* USER CODE END FunctionPrototypes */
/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init() {
  /* USER CODE BEGIN Init */
  
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  volatile size_t heapFree = xPortGetFreeHeapSize();
  
  osThreadDef(SSAnim_TaskName, SSAnim, osPriorityNormal, 0, 400);
  SSAnim_TaskHandle = osThreadCreate(osThread(SSAnim_TaskName), NULL);
  
  heapFree = xPortGetFreeHeapSize();
  
  osThreadDef(ReadCommand_TaskName, ReadCommand, osPriorityRealtime, 0, 400);
  ReadCommand_TaskHandle = osThreadCreate(osThread(ReadCommand_TaskName), NULL);
  
  //  osThreadDef(LEDsToggle_TaskName, TaskLEDsToggle, osPriorityNormal, 0, 144);
  //  LEDsToggle_TaskHandle = osThreadCreate(osThread(LEDsToggle_TaskName), NULL);
  
  heapFree = xPortGetFreeHeapSize();
  
  osThreadDef(FrameBufferUpd_TaskName, FBUpdate, osPriorityHigh, 0, 300);
  FrameBufferUpd_TaskHandle = osThreadCreate(osThread(FrameBufferUpd_TaskName), NULL);
  
  heapFree = xPortGetFreeHeapSize();
  
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* USER CODE BEGIN Application */

void SSAnim(const void *param){
  
  uint32_t period = (uint32_t)(10 / portTICK_PERIOD_MS);
  
  static portTickType animTicPos;
  
  (void)param;
  animTicPos = xTaskGetTickCount();
  
  for(;;){
    
    uint8_t red = 0, green = 0, blue = 0;
    
    float stepRes = 0.0f;
    
    stepRes = SS_step(&animSS, redState, tgtRed);
    red = (uint8_t)COERCE_VALUE(0, (int32_t)stepRes, 255);
    
    stepRes = SS_step(&animSS, greenState, tgtGreen);
    green = (uint8_t)COERCE_VALUE(0, (int32_t)stepRes, 255);
    
    stepRes = SS_step(&animSS, blueState, tgtBlue);
    blue = (uint8_t)COERCE_VALUE(0, (int32_t)stepRes, 255);
    
    if(red != tgtRed || green != tgtGreen || blue != tgtBlue)
      WS2811_push(red, green, blue);
    
    vTaskDelayUntil(&animTicPos,period);
    
  }
  
}

void TaskLEDsToggle(const void *param){
  
  uint32_t period = (uint32_t)(2000 / portTICK_PERIOD_MS);
  
  static portTickType TicPos;
  static uint8_t colorIdx = 0;
  
  (void)param;
  TicPos = xTaskGetTickCount();
  
  for(;;){
    
    if(colorIdx == 0) // RED
    {
      colorIdx = 1;
      tgtRed = 255;
      tgtGreen = 160;
      tgtBlue = 100;
    }
    else if(colorIdx == 1) // YELLOW
    {
      colorIdx = 2;
      tgtRed = 160;
      tgtGreen = 255;
      tgtBlue = 100;
    }
    else // GREEN
    {
      colorIdx = 0;
      tgtRed = 100;
      tgtGreen = 200;
      tgtBlue = 160;
    }
    
    //WS2811_push(tgtRed, tgtGreen, tgtBlue);
    
    vTaskDelayUntil(&TicPos,period);
    
  }
  
}

void ReadCommand(const void *param){
  
  uint32_t period = (uint32_t)(100 / portTICK_PERIOD_MS);
  
  (void)param;
  
  for(;;){
    
    vTaskDelay(period);
    
    if(cmdReceiveStatus == 0)
    {
      cmdReceiveStatus = 1;
      HAL_StatusTypeDef result = HAL_UART_Receive_IT(&huart1, cmdBuf, 50);
    }
    else if(cmdReceiveStatus == 2)
    {
      cmdReceiveStatus = 0;
      
      ParseAndExecuteCommand((char *)cmdBuf);
    }
  }
  
}

void FBUpdate(const void *param){
  
  uint32_t period = (uint32_t)(50 / portTICK_PERIOD_MS);
  static portTickType TicPos;
  
  (void)param;
  TicPos = xTaskGetTickCount();
  
  ssd1306Init(SSD1306_SWITCHCAPVCC);
  //ssd1306TurnOn();
  
  //drawTitle("duCka");
  ssd1306ClearScreen();
  ssd1306DrawPixel(60,32);
  ssd1306Refresh();
  HAL_Delay(200);
  ssd1306DrawPixel(64,32);
  ssd1306Refresh();
  HAL_Delay(200);
  ssd1306DrawPixel(68,32);
  ssd1306Refresh();
  HAL_Delay(200);
  ssd1306DrawPixel(62,30);
  ssd1306Refresh();
  HAL_Delay(200);
  ssd1306DrawPixel(62,32);
  ssd1306Refresh();
  HAL_Delay(200);
  ssd1306DrawPixel(62,34);
  ssd1306Refresh();
  HAL_Delay(200);
  ssd1306DrawPixel(66,30);
  ssd1306Refresh();
  HAL_Delay(200);
  ssd1306DrawPixel(66,32);
  ssd1306Refresh();
  HAL_Delay(200);
  ssd1306DrawPixel(66,34);
  ssd1306Refresh();
  
  HAL_Delay(1000);
  OLED_CLS();
  ssd1306DrawString(62-22, 28, "DUCKA", Font_System7x8);
  
  for(;;){
    
    vTaskDelayUntil(&TicPos,period);
    
    ssd1306Refresh(); // FIXME: Check if Refresh needed (some flag)
  }
  
}

void ParseAndExecuteCommand(char *cmdString)
{
  char *cmdPos;
  cmdPos = strstr(cmdString, RGB_CMD);
  if(cmdPos != NULL)
  {
    cmdPos += 4;
    
    uint32_t r, g, b;
    
    // Expecting cmdPos == '255 255 255'
    sscanf(cmdPos, "%u %u %u", &r, &g, &b);
    
    tgtRed = r & 0xFF;
    tgtGreen = g & 0xFF;
    tgtBlue = b & 0xFF;
    
    return;
  }
  
  cmdPos = strstr(cmdString, CLS_CMD);
  if(cmdPos != NULL)
  {
    cmdPos += 4;
    
    int32_t x0 = 0, y0 = 0, x1 = 0, y1 = 0, t;
    
    // Expecting cmdPos == '0 0 128 64'
    sscanf(cmdPos, "%d %d %d %d", &x0, &y0, &x1, &y1);
    
    x0 = COERCE_VALUE(0, x0, 127);
    x1 = COERCE_VALUE(0, x1, 127);
    y0 = COERCE_VALUE(0, y0, 63);
    y1 = COERCE_VALUE(0, y1, 63);
    
    if(x0 == 0 && x1 == 0 && y0 == 0 && y1 == 0)
    {
      ssd1306ClearScreen();
      return;
    }
    
    if((x0 == 0 && x1 == 0) || (y0 == 0 && y1 == 0))
    {
      return;
    }
    
    if(x0 > x1)
    {
      t = x0; x0 = x1; x1 = t;
    }
    
    if(y0 > y1)
    {
      t = y0; y0 = y1; y1 = t;
    }
    
    ssd1306FillArea(x0,y0,x1,y1,0);
    
    return;
  }
  
  cmdPos = strstr(cmdString, TXT_CMD);
  if(cmdPos != NULL)
  {
    cmdPos += 4;
    
    int32_t x = -1, y = -1, b = -1;
    char * txtDelim = ":";
    
    // Expecting cmdPos == '128,64,1:Some Text' <= [x, y, 0(small)|1(bold)]
    sscanf(cmdPos, "%d,%d,%d", &x, &y, &b);
    cmdPos = strtok(cmdPos, txtDelim);
    cmdPos = strtok(NULL, txtDelim);
    
    x = COERCE_VALUE(0, x, 127);
    y = COERCE_VALUE(0, y, 63);
    b = COERCE_VALUE(0, b, 1);
    
    ssd1306DrawString(x,y, cmdPos, (b) ? Font_System7x8 : Font_System5x8);
    
    return;
  }
  
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  cmdReceiveStatus = 2;
}

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
